<?php
/**
 * @file
 * Default theme implementation to display the Google Plus Profile.
 *
 * Variables available:
 * - $name: User's name on Google+
 * - $slogan: Status message (Slogan) on Google+.
 * - $link: Themeable link variables to the Google+ profile default page.
 * - $image: Themeable image variable information, if an user image was provided on Google+.
 * - $cover: Themeable image variable information, if an cover image was provided on Google+.
 *
 * Helper variables:
 * - $attributes_array: Array of attributes for the profile item
 * - $zebra: outputs 'odd' and 'even'.
 * - $id: Counter dependent on parent element.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * @see template_preprocess_google_plus_profile_item()
 */
?>

<div<?php print drupal_attributes($attributes_array);?>>
  <?php if ($cover || $image): ?><div class="google-plus-profile-cover">
  <?php if ($cover): print theme('image', $cover);?>
	<div class="profile-cover-overlay"></div><?php endif;?>
  </div><?php endif;?>
<?php if ($image): ?>
  <?php print theme('image', $image);?>
<?php endif;?>
  <?php print render($title_prefix); ?>
  <h6><?php print $name;?></h6>
  <?php print render($title_suffix); ?>
  <p><?php print $slogan;?></p>
  <?php print l(t('Follow '), $link, array('attributes' => array('target' => '_blank', 'class' => array('google-plus-follow-button'), 'title' => t('Follow me on Google+')), 'html' => FALSE, 'external' => TRUE));?>
</div>
