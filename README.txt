  ___   __    __    ___  __    ____    ____  __    _  _  ____
 / __) /  \  /  \  / __)(  )  (  __)  (  _ \(  )  / )( \/ ___)
( (_ \(  O )(  O )( (_ \/ (_/\ ) _)    ) __// (_/\) \/ (\___ \
 \___/ \__/  \__/  \___/\____/(____)  (__)  \____/\____/(____/

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Credits


INTRODUCTION
------------

This module adds Google Plus activity and profile info to your Website and
user profiles.


INSTALLATION
------------

1. Copy the google_plus directory to your sites/SITENAME/modules directory.

2. Enable the module at Administer >> Site building >> Modules.

3. Follow the instructions on Administer >> Configuration >> Web services >>
   Google Plus to create your Google Developer API Key.

4. Enter your Google+ User ID.

5. Enable Google Plus blocks at Administer >> Structure >> Blocks.


CREDITS
-------
* Daniel Kulbe (dku)
