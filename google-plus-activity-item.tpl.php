<?php
/**
 * @file
 * Default theme implementation to display the single Google Plus post.
 *
 * Variables available:
 * - $content: The text of the item.
 * - $published: Date when the item was published on Google+.
 * - $link: URL to the original Google+ item.
 * - $image: Themeable image variable information, if an image was provided on Google+.
 *
 * Helper variables:
 * - $attributes_array: Array of attributes for the activity item
 * - $zebra: outputs 'odd' and 'even'.
 * - $id: Counter dependent on parent element.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * @see template_preprocess_google_plus_activity_item()
 */
?>
<div<?php print drupal_attributes($attributes_array);?>>
<?php if ($image) : ?>
    <div class="image">
        <?php print theme('image', $image);?>
    </div>
<?php endif;?>
    <em class="published"><?php print $published;?></em>
    <p><?php print $content;?></p>
    <?php print l(t('Read more'), $link, array('attributes' => array('target' => '_blank', 'class' => array('google-plus-button'), 'title' => t('Read full post on Google+')), 'html' => FALSE, 'external' => TRUE));?>
</div>
