<?php
/**
 * @file
 * Google Plus module forms
 */

/**
 * Admin settings menu callback.
 *
 * @see google_plus_menu()
 */
function google_plus_settings_form ($form, &$form_state) {
  $form['#attached']['css'] = array(
    GOOGLE_PLUS_PATH . '/css/google_plus.admin.css',
  );

  $form['clear_cache'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clear cache'),
  );

  $form['clear_cache']['clear_google_plus'] = array(
    '#type' => 'submit',
    '#value' => t('Clear Google Plus cache'),
    '#submit' => array('google_plus_clear_cache_submit'),
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => variable_get('google_plus_developer_key', NULL) != NULL,
    '#collapsed' => variable_get('google_plus_developer_key', NULL) != NULL,
  );

  $tut_images = array(
    array(
      'path'   => GOOGLE_PLUS_PATH . '/images/tutorial-step-1.png',
      'width'  => 673,
      'height' => 300,
      'alt'    => t('Create new project'),
      'attributes' => array(
        'class' => array('google-plus-tutorial-image'),
      ),
    ),
    array(
      'path'   => GOOGLE_PLUS_PATH . '/images/tutorial-step-2.png',
      'width'  => 673,
      'height' => 400,
      'alt'    => t('Select API & auth section'),
      'attributes' => array(
        'class' => array('google-plus-tutorial-image'),
      ),
    ),
    array(
      'path'   => GOOGLE_PLUS_PATH . '/images/tutorial-step-3.png',
      'width'  => 673,
      'height' => 400,
      'alt'    => t('Find and enable Google+ API'),
      'attributes' => array(
        'class' => array('google-plus-tutorial-image'),
      ),
    ),
    array(
      'path'   => GOOGLE_PLUS_PATH . '/images/tutorial-step-4.png',
      'width'  => 673,
      'height' => 400,
      'alt'    => t('Create new public API key'),
      'attributes' => array(
        'class' => array('google-plus-tutorial-image'),
      ),
    ),
    array(
      'path'   => GOOGLE_PLUS_PATH . '/images/tutorial-step-5.png',
      'width'  => 673,
      'height' => 400,
      'alt'    => t('API key type selection'),
      'attributes' => array(
        'class' => array('google-plus-tutorial-image'),
      ),
    ),
    array(
      'path'   => GOOGLE_PLUS_PATH . '/images/tutorial-step-5-1.png',
      'width'  => 330,
      'height' => 182,
      'alt'    => t('Server API key form'),
      'attributes' => array(
        'class' => array('google-plus-tutorial-image'),
      ),
    ),
    array(
      'path'   => GOOGLE_PLUS_PATH . '/images/tutorial-step-5-2.png',
      'width'  => 330,
      'height' => 182,
      'alt'    => t('Browser API key form'),
      'attributes' => array(
        'class' => array('google-plus-tutorial-image'),
      ),
    ),
    array(
      'path'   => GOOGLE_PLUS_PATH . '/images/tutorial-step-6.png',
      'width'  => 673,
      'height' => 300,
      'alt'    => t('Copy new API key'),
      'attributes' => array(
        'class' => array('google-plus-tutorial-image'),
      ),
    ),
    array(
      'path'   => GOOGLE_PLUS_PATH . '/images/google-plus-id.png',
      'width'  => 600,
      'height' => 90,
      'alt'    => t('Find your Google+ ID'),
      'attributes' => array(
        'class' => array('google-plus-tutorial-image'),
      ),
    ),
  );

  $form['settings']['google_plus_devkey help'] = array(
    '#type' => 'markup',
    '#markup' =>  '<h3>' . t('Authorizing API requests') . '</h3>'.
                  '<p>' . t('Every request that your webiste sends to the Google+ API needs to identify your website to Google. Here we use the API key you get when defining your project in your !console.', array('!console' => l('Google API Console', 'https://console.developers.google.com'))) . '</p>'.

                  '<h4>' . t('Enable the Google+ API') . '</h4>'.
                  '<p>' . t('Go to the !console.', array('!console' => l('Google Developers Console', 'https://console.developers.google.com'))) . '</p>'.
                  theme_image($tut_images[0]).
                  '<p>' . t('Create a new project.') . '</p>'.
                  theme_image($tut_images[1]).
                  '<p>' . t('In the sidebar on the left, select APIs & auth. APIs is automatically selected.') . '</p>'.
                  theme_image($tut_images[2]).
                  '<p>' . t('In the displayed list of APIs, find the Google+ API service and set its status to ON.') . '</p>'.
                  theme_image($tut_images[3]).
                  '<p>' . t('In the sidebar on the left, select Credentials.') . '</p>'.
                  theme_image($tut_images[4]) . '<br />'.
                  theme_image($tut_images[5]) . '&emsp;'.
                  theme_image($tut_images[6]).
                  '<p>' . t('In the public API section create a new API key. If you want to chose the this websites IP address for identification, select "Server key", else select "Browser key".') . '</p>'.
                  theme_image($tut_images[7]).
                  '<p>' . t('Copy the key and paste it in the field below.') . '</p>'.


                  '<h4>' . t('Acquiring and using an API key') . '</h4>'.
                  '<p>' . t('For Google+ API calls that do not need to identify a particular user, you can use your application API key. To acquire an API key, visit the !accesspage. The API key is in the API Access pane\'s Simple API Access section.', array('!accesspage ' => l('Google APIs Console', 'https://code.google.com/apis/console#access'))) . '</p>'.
                  '<p>' . t('After you have an API key, your your website can append the query parameter key=yourAPIKey to all request URLs. The API key is safe for embedding in URLs; it doesn\'t need any encoding.') . '</p>'
  );

  $form['settings']['google_plus_developer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('Enter your Google Developer API Key for this website.'),
    '#default_value' => variable_get('google_plus_developer_key', NULL),
    '#size' => 60,
    '#required' => TRUE,
  );

  $form['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Website settings'),
  );

  $form['global']['google_plus_cache_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache time'),
    '#description' => t('Time to cache Google Plus activity feeds. (Avoid to hit their servers too often)'),
    '#default_value' => variable_get('google_plus_cache_time', 1800),
    '#field_suffix' => t('seconds'),
    '#size' => 10,
    '#maxlength' => 10,
    '#required' => TRUE,
  );

  $form['global']['google_plus_website_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Google+ ID'),
    '#description' => t('Enter your website\'s Google+ ID. It will be used for "Google+ website activity" and "Google+ website profile" !link.', array('!link' => l(t('blocks'), 'admin/structure/block', array('title' => t('Manage blocks'))))).'<br /><br />'.theme('image',$tut_images[8]),
    '#default_value' => variable_get('google_plus_website_id', NULL),
    '#size' => 60,
    '#required' => FALSE,
  );

  $form['activity'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Plus activity'),
  );

  $form['activity']['google_plus_max_results'] = array(
    '#type' => 'textfield',
    '#title' => t('Max results'),
    '#description' => t('Max. results for Google Plus activity feed.'),
    '#default_value' => variable_get('google_plus_max_results', 20),
    '#size' => 4,
    '#maxlength' => 2,
    '#required' => TRUE,
  );

  $form['activity']['google_plus_imagestyle'] = array(
    '#title' => t('Activity item image style'),
    '#type' => 'select',
    '#description' => t('Choose an image style to use when displaying images in Google Plus activity feed.'),
    '#options' => image_style_options(TRUE),
    '#default_value' => variable_get('google_plus_imagestyle','medium'),
  );

  $date_options = array();

  foreach (system_get_date_types() as $key => $value) {
    $date_options[$key] = $value['title'];
  }

  $form['activity']['google_plus_dateformat'] = array(
    '#title' => t('Activity item date format'),
    '#type' => 'select',
    '#description' => t('Choose an date format to use with Google Plus posts published information.'),
    '#options' => $date_options,
    '#default_value' => variable_get('google_plus_dateformat','medium'),
  );

  return system_settings_form($form);
}

/**
 * Clear Google Plus cache table
 * @see google_plus_form_system_peformance_settings_alter()
 */
function google_plus_clear_cache_submit($form, &$form_state) {
  cache_clear_all('*', 'cache_google_plus', TRUE);
  drupal_set_message(t('Google Plus caches cleared.'));
}